package de.bkBocholtWest;

public class Buch {
	
	private String titel;
	private String Autor;
	private String isbn;
	private int erscheinungsjahr;
	private double beschaffungspreis;
	private int anzahlExemplareLager;
	
	public Buch() {
		//super();
	}


	public String getTitel() {
		return titel;
	}


	public void setTitel(String titel) {
		this.titel = titel;
	}


	public String getAutor() {
		return Autor;
	}


	public void setAutor(String autor) {
		Autor = autor;
	}


	public String getIsbn() {
		return isbn;
	}


	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}


	public int getErscheinungsjahr() {
		return erscheinungsjahr;
	}


	public void setErscheinungsjahr(int erscheinungsjahr) {
		this.erscheinungsjahr = erscheinungsjahr;
	}


	public double getBeschaffungspreis() {
		return beschaffungspreis;
	}


	public void setBeschaffungspreis(double beschaffungspreis) {
		this.beschaffungspreis = beschaffungspreis;
	}


	public int getAnzahlExemplareLager() {
		return anzahlExemplareLager;
	}


	public void setAnzahlExemplareLager(int anzahlExemplareLager) {
		this.anzahlExemplareLager = anzahlExemplareLager;
	}
	
	public boolean ausleihen(int anzahl) {
		System.out.println("Sie haben " + anzahl + " Exemplare des Buchs " + this.getTitel() + " ausgeliehen.");
		return true;
	}
	
	

}
