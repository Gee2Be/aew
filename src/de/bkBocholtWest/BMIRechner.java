package de.bkBocholtWest;

public class BMIRechner {

	double koerpergroesse;
	double gewicht;

	public double bmiBerechnen() {

		double ergebnis;
		ergebnis = gewicht / (koerpergroesse * koerpergroesse);
		return ergebnis;

	}

	public String einstufungBMI(double wert) {

		if (wert <= 18.5) {
			return "Untergewicht";
		}
		if (wert > 18.5 && wert <= 24.9) {
			return "Normalgewicht";
		}
		if (wert > 24.9 && wert <= 29.9) {
			return "Übergewicht";
		}
		if (wert > 29.9 && wert <= 34.9) {
			return "Adipositas I";
		}
		if (wert > 34.9 && wert <= 39.9) {
			return "Adipositas II";
		} else {
			return "Adipositas III";
		}

	}

	public BMIRechner(double koerpergroesse, double gewicht) {
		super();
		this.koerpergroesse = koerpergroesse;
		this.gewicht = gewicht;
	}

	public static void main(String[] args) {
		
		BMIRechner bmiRechnerObjekt = new BMIRechner(1.775, 80);
		
		System.out.println( bmiRechnerObjekt.einstufungBMI( bmiRechnerObjekt.bmiBerechnen() ) );
	}

}
