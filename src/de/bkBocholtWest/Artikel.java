package de.bkBocholtWest;

public class Artikel {
	
	private String artikelname;
	private double nettopreis;
	private double steuersatz;
	
	public Artikel(String artikelname, double nettopreis, double steuersatz) {
		//super();
		this.artikelname = artikelname;
		this.nettopreis = nettopreis;
		this.steuersatz = steuersatz;
	}
	
	
	
	public String getArtikelname() {
		return artikelname;
	}



	public void setArtikelname(String artikelname) {
		this.artikelname = artikelname;
	}



	public double getNettopreis() {
		return nettopreis;
	}



	public void setNettopreis(double nettopreis) {
		this.nettopreis = nettopreis;
	}



	public double getSteuersatz() {
		return steuersatz;
	}



	public void setSteuersatz(double steuersatz) {
		this.steuersatz = steuersatz;
	}



	public double bruttopreis() {
		return nettopreis + nettopreis * steuersatz ;
		
	}

}
