package de.bkBocholtWest;

public class Bibliothek {
	
	public void hinzufuegen(Buch buch) {
		System.out.println("Sie haben das Buch " + buch.getTitel() + " hinzugefügt.");
	}
	
	public void buchAusleihen(String isbn, int anzahl) {
		System.out.println("Sie haben " + anzahl + " Bücher mit des isbn " + isbn + " ausgehliehen.");
	}
	
	public void rueckgabe(String isbn, int anzahl, boolean puenktlich) {
		System.out.println("Sie haben " + anzahl + " Bücher mit der isbn " + isbn + " pünktlich (" + puenktlich + ") zurückgegeben.");
	}
	
	public Bibliothek() {
		
	}

}
