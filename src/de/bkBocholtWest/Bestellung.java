package de.bkBocholtWest;

public class Bestellung {
	
	private Artikel artikel;
	private int bestellmenge;
	
	public Bestellung(Artikel artikel, int bestellmenge) {
		super();
		this.artikel = artikel;
		this.bestellmenge = bestellmenge;
	}
	
	public double gesamtBruttopreis() {
		return artikel.bruttopreis() * bestellmenge;
	}

}
