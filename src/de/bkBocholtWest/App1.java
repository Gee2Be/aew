package de.bkBocholtWest;

public class App1 {
	
	public static void main(String[] args) {
		
		Buch erstesBuch = new Buch();
		erstesBuch.setTitel("Harry Potter und ein Stein");
		erstesBuch.setIsbn("1");
		erstesBuch.setAutor("J.K. Rowling");
		erstesBuch.setAnzahlExemplareLager(3);
		erstesBuch.setErscheinungsjahr(2000);
		erstesBuch.setBeschaffungspreis(12.00);
		
		Buch zweitesBuch = new Buch();
		zweitesBuch.setTitel("Harry Potter und die Schlangenhöhle");
		zweitesBuch.setIsbn("2");
		zweitesBuch.setAutor("J.K. Rowling");
		zweitesBuch.setAnzahlExemplareLager(5);
		zweitesBuch.setErscheinungsjahr(2001);
		zweitesBuch.setBeschaffungspreis(14.00);
		
		Buch drittesBuch = new Buch();
		drittesBuch.setTitel("Harry Potter und irgendwas");
		drittesBuch.setIsbn("3");
		drittesBuch.setAutor("J.K. Rowling");
		drittesBuch.setAnzahlExemplareLager(20);
		drittesBuch.setErscheinungsjahr(2004);
		drittesBuch.setBeschaffungspreis(22.00);
		
		erstesBuch.ausleihen(2);
		zweitesBuch.ausleihen(3);
		drittesBuch.ausleihen(5);
		
		Bibliothek ersteBibliothek = new Bibliothek();
		
		ersteBibliothek.hinzufuegen(erstesBuch);
		ersteBibliothek.hinzufuegen(zweitesBuch);
		ersteBibliothek.hinzufuegen(drittesBuch);
		
		ersteBibliothek.buchAusleihen("1", 2);
		ersteBibliothek.buchAusleihen("2", 3);
		ersteBibliothek.buchAusleihen("3", 5);
		
		ersteBibliothek.rueckgabe("1", 2, true);
		ersteBibliothek.rueckgabe("2", 3, true);
		ersteBibliothek.rueckgabe("3", 5, true);
		
		
		
	}

}
