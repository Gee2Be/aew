package de.bkBocholtWest;

import java.util.Date;

public class Auction {
	
	public long auctionIdent;
	protected String title;
	private double bestBid;
	private int numberOfBids;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getBestBid() {
		return bestBid;
	}

	public void setBestBid(double bestBid) {
		this.bestBid = bestBid;
	}

	public long getAuctionIdent() {
		return auctionIdent;
	}

	public void setNumberOfBids(int numberOfBids) {
		this.numberOfBids = numberOfBids;
	}

	public int getNumberOfBids() {
		return 1;
	}
	
	protected static void incNumberOfBids() {
		
	}
	
	public void bid(String person, Date time, double money) {
		
	}
	
	public void getAuctionStatus() {
		
	}
	
	private void setAuctionIdent(long id) {
		
	}

	public Auction(long auctionIdent, String title, double bestBid, int numberOfBids) {
		super();
		this.auctionIdent = auctionIdent;
		this.title = title;
		this.bestBid = bestBid;
		this.numberOfBids = numberOfBids;
	}
	
	

}
